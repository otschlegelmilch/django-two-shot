from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from .models import Account, ExpenseCategory, Receipt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect


# Create your views here.

# Protect the list view for the Receipt model
# so that only a person who has logged in can access it. (LoginRequired)
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/create.html"

    # a method that assigns the current user to a User property
    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = (
            self.request.user
        )  # use 'purchaser' because it's the foreign key with User in it
        item.save()
        return redirect("receipts/list.html")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "categories/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("/receipts/categories/")


class AccountCreateView(CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "accounts/create.html"
